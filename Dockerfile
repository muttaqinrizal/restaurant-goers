FROM haqqer/node-python-12 AS ts-builder

WORKDIR /home/node/app

#RUN apk --no-cache add --virtual builds-deps build-base python

COPY . .

RUN npm install

RUN npm run build

FROM haqqer/node-python-12 AS ts-runner

WORKDIR /home/node/app 

COPY . .

COPY --from=ts-builder /home/node/app/node_modules ./node_modules

COPY --from=ts-builder /home/node/app/dist ./dist

EXPOSE 3000

CMD ["node","."]
