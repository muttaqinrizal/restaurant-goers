# Controller

## Struktur Controller

Yang cukup berbeda dari controller di project ini dan project-project sebelumnya adalah kita memisah 1 fungsional ke dalam controller yang terpisah. Misalkan pada modul employee terdapat 5 controller sebagai berikut

1. employee.controller.ts
1. employeeCreate.controller.ts
1. employeeDetail.controller.ts
1. employeeList.controller.ts
1. employeeUpdate.controller.ts

`employee.controller.ts` digunakan apabila ada middleware-middleware umum yang digunakan untuk seluruh controller di dalam employee. Pada kasus kali ini, middleware yang dipakai adalah `isAuthenticated` yang mana itu berarti seluruh fungsi-fungsi pada modul Employee hanya bisa diakses oleh user yang sudah login.

`employeeXXX.controller.ts` digunakan untuk 1 fungsionalitas di dalam modul Employee. Hal ini ditujukan supaya file controller tidak terlalu besar dan sulit untuk dimanage. Salah satu controller yang akan saya bahas di sini adalah `employeeUpdate.controller.ts`

### Employee Update Controller

Pada controller ini hal-hal terkait dengan update, termasuk menampilkan view update dan updatenya dimasukkan ke controller ini. Contohnya adalah seperti di bawah ini.

```
employeeUpdateController.get(
  '/:id/update',
  async (req: Request, res: Response) => {
    ...
  },
)

employeeUpdateController.post(
  '/:id/update',
  async (req: Request, res: Response) => {
    ...
  },
)
```

Kasus lain yang baiknya dijadikan 1 controller adalah untuk membuat tabel menggunakan datatable yang datanya diambil dari API. Dalam case ini, menampilkan halaman list dan api untuk datatablenya dibuat dalam 1 file.

## Error Handling

Karena hampir seluruh codebase yang kita maintain menggunakan `async/await`, maka selalu gunakan method `asyncHandler` supaya seluruh error dapat tertangkap, tercatat di log dan tertampil halaman yang sesuai. Berikut ini adalah contoh penggunaannya

```
controller.get('/', asyncHandler(async (req: Request, res: Response) => {
  ...
}))
```

## Response

Memaintain 2 codebase untuk API (yang nantinya akand igunakan pada aplikasi) dan aplikasi web merupakan tugas yang cukup sulit. Sehingga duplikasi kode yang tidak diperlukan, diusahakan supaya tidak terjadi, oleh karena itu dibuat function `responseHandler` agar controller yang dibuat dapat digunakan baik di API maupun di web. Berikut ini adalah beberapa contoh kasus penggunaan response handler tersebut

1. Render template

   ```
   responseHandler(res, {
     responseType: ResponseType.SUCCESS,
     templatePath: '/pages/test',
     data: {
       test: 'test',
       hello: []
     }
   });
   ```

2. Redirect ke halaman lain

   ```
   responseHandler(res, {
     responseType: ResponseType.SUCCESS,
     redirectUrl: '/master/pegawai',
   });
   ```

3. Render error page

   Untuk menampilkan halaman error,

## Validation

Dalam membuat aplikasi, seringkali

### redirectBackIfError

Handler ini biasa digu

### redirectBackIfEmpty
