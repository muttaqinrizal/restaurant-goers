'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    gender: DataTypes.STRING,
    birthOfDate: DataTypes.STRING,
    location: DataTypes.STRING,
    photo: DataTypes.STRING,
    email: DataTypes.STRING,
    linkedin: DataTypes.STRING,
    password: DataTypes.STRING,
    handphone: DataTypes.STRING,
    isAdmin: DataTypes.STRING,
    countPending: DataTypes.INTEGER,
    countUpComing: DataTypes.INTEGER,
    countRejected: DataTypes.INTEGER,
    countCompleted: DataTypes.INTEGER,
  }, {});
  User.associate = function(models) {
    // associations can be defined here
  };
  return User;
};