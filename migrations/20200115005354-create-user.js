'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      email: {
        type: Sequelize.STRING,
        unique: true,
      },
      password: {
        type: Sequelize.STRING
      },
      name: {
        type: Sequelize.STRING,
      },
      gender: {
        type: Sequelize.STRING,
      },
      birthOfDate: {
        type: Sequelize.DATE,
      },
      location: {
        type: Sequelize.STRING,
      },
      province: {
        type: Sequelize.STRING,
      },
      residence: {
        type: Sequelize.STRING,
      },
      district: {
        type: Sequelize.STRING,
      },
      photo: { 
        type: Sequelize.STRING,
      },
      linkedin: {
        type: Sequelize.STRING,
      },
      google: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
      },
      roles: {
        type: Sequelize.STRING,
        defaultValue: 'user'
      },
      bankName: {
        type:Sequelize.STRING,
      },
      accountNumber: {
        type: Sequelize.STRING,
      },
      accountName: {
        type: Sequelize.STRING,
      },
      countPending: {
        type: Sequelize.INTEGER,
      },
      countUpComing: {
        type: Sequelize.INTEGER,
      },
      countRejected: {
        type: Sequelize.INTEGER,
      },
      countCompleted: {
        type: Sequelize.INTEGER
      },
      verified: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};