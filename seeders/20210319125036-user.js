'use strict';
const moment = require('moment');
const bcrypt = require('bcrypt');

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('Users', [{
      email: 'cs@temuexpert.id',
      password: bcrypt.hashSync('secret', 256),
      name: 'customer service',
      gender: 'Laki-laki',
      birthOfDate: '1999-12-10 10:00:00',
      location: 'Semarang',
      photo: '',
      linkedin: '',
      phone: '+01 23456789',
      roles: 'admin',
      countPending: 0,
      countUpComing: 0,
      countRejected: 0,
      countCompleted: 0,
      verified: true,
      createdAt: '2021-03-18 20:00:00',
      updatedAt: '2021-03-18 20:00:00'
    }], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
