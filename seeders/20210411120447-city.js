'use strict';
const fs = require('fs')
const path = require('path')
module.exports = {
  up: (queryInterface, Sequelize) => {
    const filePath = path.join(__dirname, 'kota.sql')
    const result = fs.readFileSync(filePath, { encoding: 'utf-8' })
    return queryInterface.sequelize.query(result);
  },
  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
