import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import expressStatusMonitor from 'express-status-monitor';
import moment from 'moment';
import 'moment/locale/id';
import morgan from 'morgan';
import numeral from 'numeral';
import path from 'path';
import 'reflect-metadata';
import { logger } from './helpers/logger';
import { pathMiddleware } from './modules/core/middlewares/path.middleware';
moment().locale('id');

const middlewareLoader = (app: express.Application) => {
  app.use(expressStatusMonitor());
  app.use(compression());
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(cors())
  app.options('*', cors())
  app.use(
    morgan('tiny', {
      stream: {
        write: message => {
          logger.info(message);
        },
      },
    }),
  );
  app.use(cookieParser());
  // app.use(passport.initialize());
  // app.use(passport.session());
  // app.use(flash());
  // app.use((req, res, next) => {
    // if (req.is('multipart/form-data')) {
    //   next();
    // }
  //   if (req.path.split('/')[1] === 'api' || req.is('multipart/form-data')) {
  //     next();
  //   } else {
  //     lusca.csrf()(req, res, next);
  //   }
  // });
  // app.use(lusca.xframe('SAMEORIGIN'));
  // app.use(lusca.xssProtection(true));
  app.disable('x-powered-by');
  app.use(async (req, res, next) => {
    res.locals.user = req.user;
    moment.locale('id');
    res.locals.moment = moment;
    res.locals.numeral = numeral();
    next();
  });
  app.use('/file', express.static(path.join(__dirname, '../file')));
  app.use(pathMiddleware);
};

export = middlewareLoader;
