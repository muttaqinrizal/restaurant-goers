import { Request } from 'express';
import { User } from '../modules/master/user/user.model';
export interface Request extends Request {
  user: User;
  userRoles: object;
  swaggerDoc: any;
}
