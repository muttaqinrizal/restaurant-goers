import { NextFunction, Router } from 'express';
import { apiAuthentication } from '../modules/core/config/passport';
import AuthApiController from '../modules/core/controllers/auth.api.controller';
import { fileMiddleware } from '../modules/core/middlewares/file.middleware';
import CityController from '../modules/master/city/city.controller';
import ProvinceController from '../modules/master/province/province.controller';
import UserController from '../modules/master/user/user.controller';

function group(cb: any) {
  const route = Router()
  cb(route)
  return route  
}

const appRouter = Router();

appRouter.use('/auth', group((router: Router, next: NextFunction) => {
  router.post('/login', AuthApiController.login);
  router.post('/google', AuthApiController.google);
  router.post('/linkedin', AuthApiController.linkedin );
  router.post('/register', AuthApiController.register);
  router.get('/verification', AuthApiController.verification);
  router.post('/forgot-password', AuthApiController.forgotPassword);
  router.post('/reset-password', AuthApiController.resetPassword);
  router.post('/resend-verification', AuthApiController.resendVerificationLink);
  router.get('/me', apiAuthentication, AuthApiController.me);
}))

appRouter.use('/user', apiAuthentication, group((router: Router, next: NextFunction) => {
  router.get('/', UserController.index)
  router.get('/profile', UserController.profile)
  router.patch('/profile', fileMiddleware({ fields: [{ name: 'photo', maxCount: 1 }] }), UserController.updateProfile)
  router.patch('/account', UserController.updateAccount)
}))

appRouter.use('/province', group((router: Router, next: NextFunction)=>{
  router.get('/', ProvinceController.index)
  router.get('/:id/city', CityController.index)
}))


export default appRouter