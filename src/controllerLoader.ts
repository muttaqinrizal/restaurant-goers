import express from 'express';
import 'reflect-metadata';
import { apiController } from './apiControllerLoader';


const controllerLoader = (app: express.Application) => {
  app.use(apiController);
};

export = controllerLoader;
