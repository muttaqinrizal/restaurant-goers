import 'reflect-metadata';
import { Sequelize } from 'sequelize-typescript';
import { City } from './modules/master/city/city.model';
import { Province } from './modules/master/province/province.model';
import { User } from './modules/master/user/user.model';


const modelLoader = (sequelize: Sequelize) => {
  sequelize.addModels([
    User,
    Province,
    City,
  ]);
};

export = modelLoader;
