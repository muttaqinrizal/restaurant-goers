import { Op } from 'sequelize';
import { Province } from '../province/province.model';
import { City } from './city.model';

export class CityRepository{
  public static async getAll(id: number, search: any = ''): Promise<City[]>{
    const data = await City.findAll({
      include: [
        {
          model: Province,
        },
      ],
      where:{
        provinceId: id,
        name: {
          [Op.like]: `%${search}%`,
        },
      },
    })
    return data
  }
}