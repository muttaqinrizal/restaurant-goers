import { BelongsTo, Column, ForeignKey, Model, Table } from 'sequelize-typescript';
import { Province } from '../province/province.model';

@Table({
  tableName:'Cities',
  timestamps: true,
  paranoid: true,
})

class City extends Model<City>{
  @Column
  public name: string

  @ForeignKey(()=> Province)
  @Column
  public provinceId: number

  @BelongsTo(()=> Province)
  public province: Province[]

  @Column
  public latitude: string

  @Column
  public longitude: string
}

export { City };

