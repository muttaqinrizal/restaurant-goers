import { NextFunction, Response } from 'express';
import { Request } from '../../../typings/request';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { CityRepository } from './city.repository';

export default class CityController{
  public static index = async(req:Request, res: Response, next: NextFunction) =>{
    try {
      console.log(req.params.id);
      
      const data = await CityRepository.getAll(parseInt(req.params.id, 10), req.query.search)
      return ResponseHandler.jsonSuccess(res, data)
    } catch (error) {
      return ResponseHandler.serverError(res, error)
    }
  }
}