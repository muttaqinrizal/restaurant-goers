import { Column, HasMany, Model, Table } from 'sequelize-typescript';
import { City } from '../city/city.model';

@Table({
  tableName:'Provinces',
  timestamps: true,
  paranoid: true,
})

class Province extends Model<Province>{
  @HasMany(()=> City)
  public city: City[]
  
  @Column
  public name: string

  @Column
  public latitude: string

  @Column
  public longitude: string
}

export { Province };

