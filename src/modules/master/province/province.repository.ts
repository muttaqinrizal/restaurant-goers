import { Op } from 'sequelize';
import { Province } from './province.model';

export class ProvinceRepository{
  public static async getAll(search: any = ''): Promise<Province[]>{
    const data = await Province.findAll({
      where:{
        name : {
          [Op.like]: `%${search}%`,
        },
      },
    })
    return data   
  }
}