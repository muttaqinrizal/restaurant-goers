import { NextFunction, Response } from 'express';
import { Request } from '../../../typings/request';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { ProvinceRepository } from './province.repository';

export default class ProvinceController {
  public static index = async(req: Request, res: Response, next: NextFunction) => {
    try {
      const data = await ProvinceRepository.getAll(req.query.search)
      return ResponseHandler.jsonSuccess(res, data)
    } catch (error) {
      return ResponseHandler.serverError(res, 'hahah' + error)
    }
  }
}