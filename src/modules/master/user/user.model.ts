import { Column, Model, Table, Unique } from 'sequelize-typescript';

@Table({
  tableName: 'Users',
  timestamps: true,
  paranoid: true,
})

class User extends Model<User>{
  @Unique
  @Column
  public email: string;
  
  @Column
  public password: string;
  
  @Column
  public name: string;

  @Column
  public gender: string;

  @Column
  public birthOfDate: Date;

  @Column
  public location: string;

  @Column
  public photo: string;

  @Column
  public linkedin: string;
  
  @Column
  public google: string;

  @Column
  public phone: string;

  @Column({defaultValue: 'user'})
  public roles: string;

  @Column
  public bankName: string;

  @Column
  public accountNumber: string;

  @Column
  public accountName: string;

  @Column
  public province: string;

  @Column
  public residence: string;

  @Column
  public district: string;

  @Column
  public countPending: number;

  @Column
  public countUpComing: number;

  @Column
  public countRejected: number;

  @Column
  public countCompleted: number;

  @Column({ defaultValue: false })
  public verified: boolean;

  @Column
  public createdAt: Date;

  @Column
  public updatedAt: Date;

  @Column
  public deletedAt: Date;
}

export { User };


