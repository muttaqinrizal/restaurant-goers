import bcrypt from 'bcrypt';
import { User } from './user.model';

export class UserRepository {

  public static async get(): Promise<User[]> {
    const offset: number = this.offset
    const limit: number = this.limit
    const where: {} = this.where
    const order: [any] = this.order
    const data = await User.findAll({where, order, limit, offset})
    return data
  }

  public static async getDetail(id:number): Promise<User>{
    const data = await User.findByPk(id)
    return data
  }

  public static async getByEmail(email: string): Promise<User>{
    const data = await User.findOne({
      where: { email },
      attributes: {
        exclude: ['password'],
      },
    })
    return data
  }

  public static async getByGoogleID(googleId: string): Promise<User>{
    const data = await User.findOne({
      where: { google: googleId },
      attributes: {
        exclude: ['password'],
      },
    })
    return data
  }

  public static async getByLinkedinID(linkedinId: string): Promise<User>{
    const data = await User.findOne({
      where: { linkedin: linkedinId },
      attributes: {
        exclude: ['password'],
      },
    })
    return data
  }
  
  public static async create(data: any): Promise<User>{
    const user = await User.create({...data})
    return user
  }

  public static async updateUser(id: number, data: any): Promise<User> {
    const { name, birthOfDate, gender, province, residence, district, location, photo } = data
    await User.update({...{ name, birthOfDate, gender, province, residence, district, location, photo }}, {where: {id}});
    const user = await User.findByPk(id)
    return user
  }

  public static async updateAccount(id: number, data: any): Promise<User> {
    const { email, password, phone, bankName, accountNumber, accountName } = data
    if (password) {
      const encryptedPassword = bcrypt.hashSync(password, 256)
      await User.update({...{ email, password: encryptedPassword, phone, bankName, accountNumber, accountName }}, {where:{id}})
    } else {
      await User.update({...{ email, phone, bankName, accountNumber, accountName }}, {where:{id}})
    }
    const user = await User.findByPk(id)
    return user
  }

  public static async changeRole(id: number, roles: string): Promise<User>{
    await User.update({roles},{where:{id}})
    const data = await User.findByPk(id)
    return data
  }

  protected static limit: number = 10;
  protected static offset: number = 0;
  protected static where: {};
  protected static include: object[] = [];
  protected static order: [
    ['name', 'ASC']
  ]
}