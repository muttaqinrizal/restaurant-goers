import { NextFunction, Request, Response } from 'express';
import { ResponseHandler } from '../../core/helpers/responseHandler';
import { UserRepository } from './user.repository';
import { AccountUpdateRequestSchema, UserUpdateRequestSchema } from './user.schema';

export default class UserController {
  public static async index(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      // const profile = await UserRepository.get()
      // const data = { req }
      return ResponseHandler.jsonSuccess(res, {});
    } catch (error) {
      next(error)
    }
  }

  public static async profile(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const data = await UserRepository.getDetail(req.user.id)
      return ResponseHandler.jsonSuccess(res, data)
    } catch (error) {
      return ResponseHandler.serverError(res, 'Gagal mengambil data')
    }
  }

  public static async updateProfile(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const file: any = req.files
      if (file.photo) {
        req.body.photo = file.photo[0].filename
      }
      const { value, error } = UserUpdateRequestSchema.validate(req.body)
      if (error) { return ResponseHandler.jsonError(res, 'Validasi error '+ error)}
      await UserRepository.updateUser(req.user.id, value)
      const data = await UserRepository.getDetail(req.user.id);
      return ResponseHandler.jsonSuccess(res, data, 'Berhasil melakukan update data')
    } catch (error) {
      return ResponseHandler.serverError(res, 'Gagal mengupdate data')
    }
  }

  public static async updateAccount(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { value, error } = AccountUpdateRequestSchema.validate(req.body)
      if (error) { return ResponseHandler.jsonError(res, 'Validasi error' + error)}
      const data = await UserRepository.updateAccount(req.user.id, value)
      return ResponseHandler.jsonSuccess(res, data, 'Berhasil melakukan update data')
    } catch (error) {
      return ResponseHandler.serverError(res, 'Gagal mengupdate data')
    }
  }

  public static async changeRole(req:Request, res: Response, next: NextFunction): Promise<void>{
    try {
      const { roles } = req.body
      const data = await UserRepository.changeRole(req.user.id, roles)
      return ResponseHandler.jsonSuccess(res, data, 'Berhasil mengupdate data')
    } catch (error) {
      return ResponseHandler.serverError(res, 'Gagal mengupdate data')
    }
  }
}