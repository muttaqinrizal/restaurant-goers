import Joi from 'typesafe-joi'

export const UserRequestSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().min(8).required(),
  name: Joi.string().required(),
  gender: Joi.string(),
  birthOfDate: Joi.date(),
  location: Joi.string(),
  photo: Joi.string(),
  linkedin: Joi.string(),
  phone: Joi.string(),
  roles: Joi.string(),
  countPending: Joi.number(),
  countUpComing: Joi.number(),
  countRejected: Joi.number(),
  countCompleted: Joi.number(),
  verified: Joi.bool(),
})

export const UserUpdateRequestSchema = Joi.object({
  name: Joi.string(),
  birthOfDate: Joi.date(),
  gender: Joi.string(),
  province: Joi.string(),
  residence: Joi.string(),
  district: Joi.string(),
  location: Joi.string(),
  photo: Joi.string(),
})

export const AccountUpdateRequestSchema = Joi.object({
  email: Joi.string(),
  password: Joi.string().allow(null, ''),
  phone: Joi.string(),
  bankName: Joi.string(),
  accountNumber: Joi.string(),
  accountName: Joi.string(),
})

export type UserCreateSchema = Joi.Literal<typeof UserRequestSchema>
export type UserUpdateRequestSchema = Joi.Literal<typeof UserUpdateRequestSchema>
export type AccountUpdateRequestSchema = Joi.Literal<typeof AccountUpdateRequestSchema>