export class ResponseType {

  public static SUCCESS = new ResponseType(200, false, 'Success');
  public static NOT_FOUND = new ResponseType(404, true, 'Not found');
  public static FORBIDDEN = new ResponseType(400, true, 'Forbidden');
  public static UNAUTHORIZED = new ResponseType(401, true, 'Unauthorized');
  public static SERVER_ERROR = new ResponseType(500, true, 'Server Error');
  public static VALIDATION_ERROR = new ResponseType(422, true, 'Validation Error');
  public status: number;
  public error: boolean;
  public message: string;

  constructor(status: number, error: boolean, message: string) {
    this.status = status;
    this.error = error;
    this.message = message;
  }
}