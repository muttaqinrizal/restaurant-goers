import axios from 'axios';
import { OAuth2Client } from 'google-auth-library';
import qs from 'qs';

export class OauthClientHandler {  
  public static async getGoogleProfile(idToken: string) {
    const client = new OAuth2Client(process.env.GOOGLE_CLIENT_ID);
    const verifyToken = await client.verifyIdToken({
        idToken,
        audience: process.env.GOOGLE_CLIENT_ID,
    });
    const profile = verifyToken.getPayload();
    return profile;
  }

  public static async getLinkedinProfile(code: string) {
    const document = {
      'grant_type': 'authorization_code',
      'code': code,
      'redirect_uri': process.env.LINKEDIN_REDIRECT_URI,
      'client_id': process.env.LINKEDIN_CLIENT_ID,
      'client_secret': process.env.LINKEDIN_CLIENT_SECRET,
    }
    const { data } = await axios.post('https://www.linkedin.com/oauth/v2/accessToken',
      qs.stringify(document),
      { headers: { 'content-type': 'application/x-www-form-urlencoded' },
     },
    )
    axios.defaults.headers.common.Authorization = `Bearer ${data.access_token}`;
    const linkedinData = await axios.all([
      axios.get('https://api.linkedin.com/v2/emailAddress?q=members&projection=(elements*(handle~))'),
      axios.get('https://api.linkedin.com/v2/me?projection=(id,localizedFirstName,localizedLastName)'),
    ])
    const email = linkedinData[0].data.elements[0]['handle~'].emailAddress
    const profile = linkedinData[1].data
    const result = {
      email,
      name: `${profile.localizedFirstName} ${profile.localizedLastName}`,
      linkedin: profile.id,
      verified: true,
    }
    return result;
  }
}