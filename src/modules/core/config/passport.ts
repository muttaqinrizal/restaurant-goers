import { NextFunction, Response } from 'express';
import jwt from 'jsonwebtoken';
import passport from 'passport';
import { Request } from '../../../typings/request';
import { ErrorThrower } from '../helpers/errorThrower';
import { ResponseType } from '../helpers/responseType';

/**
 * Login Required middleware.
 */
const apiAuthentication = (req: Request, res: Response, next: NextFunction) => {
  if (!req.headers.authorization && !req.headers.auth) {
    throw new ErrorThrower({ message: 'NO_TOKEN', responseType: ResponseType.UNAUTHORIZED });
  }
  let token = req.headers.authorization || req.headers.auth.toString();
  token = token.slice(7, token.length);
  jwt.verify(token, process.env.SESSION_SECRET, (error: any, decoded: any) => {
    if (error || decoded === null) {
      throw new ErrorThrower({ message: 'Token Not Valid', responseType: ResponseType.FORBIDDEN });
    } else {
      req.user = decoded;
      return next();
    }
  });
};

const isAuthenticated = (req: Request, res: Response, next: NextFunction) => {
  if (process.env.AUTH_DISABLED === 'yes') {
    return next();
  }
  if (res.locals.api === true) {
    return apiAuthentication(req, res, next);
  }
};

export { isAuthenticated, passport, apiAuthentication };

