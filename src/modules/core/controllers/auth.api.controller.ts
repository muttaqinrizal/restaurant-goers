import bcrypt from 'bcrypt';
import { Response } from 'express';
import jwt from 'jsonwebtoken';
import mjml2html from 'mjml';
import { Request } from '../../../typings/request';
import { User } from '../../master/user/user.model';
import { UserRepository } from '../../master/user/user.repository';
import { UserRequestSchema } from '../../master/user/user.schema';
import { Mailer } from '../config/mailer';
import { OauthClientHandler } from '../helpers/oauthClientHandler';
import { ResponseHandler } from '../helpers/responseHandler';
import { getMjmlRegister, getMjmlResetPassword } from './auth.helper';


export default class AuthApiController {
  public static login = async (req: Request, res: Response) => {
    try {
      const { email } = req.body;
      
      const user = await User.findOne({
        where: { email },
        raw: true,
      })
      if (!user) {
        return ResponseHandler.jsonError(res, 'User tidak ditemukan')
      }
      
      const passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
      if (!passwordIsValid) {
        return ResponseHandler.jsonError(res, 'password tidak sesuai')
      }
      if (!user.verified) { return ResponseHandler.jsonError(res, 'user belum terverifikasi') }
      const { password, ...result } = user;
      const token = jwt.sign(result, process.env.SESSION_SECRET);
      const data = {
        token,
        result,
      }
      return ResponseHandler.jsonSuccess(res, data, 'Login Berhasil')
    } catch (error) {
      console.log(error)
      return ResponseHandler.serverError(res, 'Gagal Login')
    }
  }

  public static me = async (req: Request, res: Response) => {
    const user = req.user
    const data = {
      user,
    }
    return ResponseHandler.jsonSuccess(res, data)
  }

  public static google = async (req: Request, res: Response) => {
    try {
      const profile = await OauthClientHandler.getGoogleProfile(req.body.idToken);
      const user = await UserRepository.getByEmail(profile.email);
      const userByGoogleId = await UserRepository.getByGoogleID(profile.sub);
      if (user && !userByGoogleId) {
        await User.update(
          { 
            google: profile.sub,           
          },
          { 
            where: { id: user.id },
          },
        )
      }
      if (!user && !userByGoogleId) {
        const {value} = UserRequestSchema.validate({ name: profile.name, email: profile.email, photo: profile.picture, google: profile.sub, verified: true });
        await UserRepository.create(value)
      }
      const result = await UserRepository.getByGoogleID(profile.sub); 
      const token = jwt.sign(JSON.stringify(result), process.env.SESSION_SECRET);
      const data = {
        token,
        result,
      }
      return ResponseHandler.jsonSuccess(res, data, 'Login Google berhasil')
    } catch (error) {
      if (error.message.includes('Token used too late')) {
        return ResponseHandler.unauthorized(res, 'Token Habis')
      }
      return ResponseHandler.serverError(res, 'Gagal Login Google')
    }
  }

  public static linkedin = async (req: Request, res: Response) => {
    try {
      const profile = await OauthClientHandler.getLinkedinProfile(req.body.code);
      const user = await UserRepository.getByEmail(profile.email);
      const userByLinkedinId = await UserRepository.getByLinkedinID(profile.linkedin);
      if (user && !userByLinkedinId) {
        await User.update(
          { 
            linkedin: profile.linkedin,           
          },
          { 
            where: { id: user.id },
          },
        )
      }
      if (!user && !userByLinkedinId) {
        const {value} = UserRequestSchema.validate(profile);
        await UserRepository.create(value)
      }
      const result = await UserRepository.getByLinkedinID(profile.linkedin);
      const token = jwt.sign(JSON.stringify(result), process.env.SESSION_SECRET);
      const data = {
        token,
        result,
      }
      return ResponseHandler.jsonSuccess(res, data, 'Login Linkedin berhasil')
    } catch (error) {
      console.log(error)
      return ResponseHandler.serverError(res, 'Gagal Login Linkedin')
    }
  }

  public static register = async (req: Request, res: Response) => {
    const { email, password  } = req.body;
    const encryptedPassword = bcrypt.hashSync(password, 256)
    try {
      const { error, value } = UserRequestSchema.validate(req.body)
      if (error) { return ResponseHandler.jsonError(res, error.message) }
      const user = await UserRepository.getByEmail(email);
      if (user) {
        return ResponseHandler.jsonError(res, 'Alamat email sudah terdaftar. Silakan masukkan alamat email lain')
      }
      value.password = encryptedPassword;
      await UserRepository.create(value);
      const result = await UserRepository.getByEmail(email);
      const token = jwt.sign(JSON.stringify(result), process.env.SESSION_SECRET);
      const verificationLink = `${process.env.BASE_URL}/auth/verification?code=${token}`;
      const mjmlResult = mjml2html(getMjmlRegister(result, verificationLink));
      const html = mjmlResult.html;
      await Mailer.sendEmail({
        to: email,
        subject: 'Verifikasi akun',
        html,
      });
      const data = {
        token,
        result,
      }
      return ResponseHandler.jsonSuccess(res, data, 'Register berhasil');
    } catch (error) {
      console.log(error.message);
      return ResponseHandler.serverError(res, 'Gagal register');
    }
  }

  public static forgotPassword = async (req: Request, res: Response) => {
    const { email } = req.body;
    const user = await UserRepository.getByEmail(email);
    if (!user) {
      return ResponseHandler.jsonError(res, 'User tidak ditemukan');
    }
    const token = jwt.sign(JSON.stringify(user), process.env.SESSION_SECRET);
    
    const linkResetPassword = `${process.env.BASE_URL}/auth/reset-password/?token=${token}`;
    
    const mjmlResult = mjml2html(getMjmlResetPassword(user, linkResetPassword));
    const html = mjmlResult.html;

    const sendEmailResult = await Mailer.sendEmail({
      to: user.email,
      subject: 'Reset Password Akun',
      html,
    });
    
    console.log('succes message', sendEmailResult)
    return ResponseHandler.jsonSuccess(res, { sendEmailResult }, 'Link reset password telah berhasil dikirim ke email Anda');
  }

  public static resetPassword = async (req: Request, res: Response) => {
    try {
      const user = await UserRepository.getByEmail(req.body.email);
      
      if (!user) {
        return ResponseHandler.jsonError(res, 'User Tidak ditemukan');
      }
      
      await User.update(
        { 
          password: await bcrypt.hash(req.body.password, 256),           
        },
        { 
          where: { id: user.id },
        },
      )
      
      return ResponseHandler.jsonSuccess(res, { user }, 'update password berhasil');
    } catch (error) {
      return ResponseHandler.serverError(res);
    } 
  }
  public static verification = async (req: Request, res: Response) => {
    try {
      const token = req.query.code.toString();
      const decoded: any = jwt.verify(token, process.env.SESSION_SECRET);
      const user = await UserRepository.getByEmail(decoded.email)
      if (!user) {
        return ResponseHandler.jsonError(res, 'User Tidak ditemukan');
      }
      await User.update(
        { 
          verified: true,           
        },
        { 
          where: { id: user.id },
        },
      )
      const result = await UserRepository.getDetail(user.id)
      return ResponseHandler.jsonSuccess(res, { result }, 'verifikasi berhasil');
    } catch (error) {
      return ResponseHandler.serverError(res);
    } 
  }
  public static resendVerificationLink = async (req: Request, res: Response) => {
    try {
      const { email } = req.body;
      const user = await UserRepository.getByEmail(email);
      if (!user) {
        return ResponseHandler.jsonError(res, 'User tidak ditemukan');
      }
      if (user.verified) {
        return ResponseHandler.jsonError(res, 'User telah terverfikasi');
      }
      const token = jwt.sign(JSON.stringify(user), process.env.SESSION_SECRET);
      const verificationLink = `${process.env.BASE_URL}/auth/verification?code=${token}`;
      const mjmlResult = mjml2html(getMjmlRegister(user, verificationLink));
      const html = mjmlResult.html;
      await Mailer.sendEmail({
        to: email,
        subject: 'Verifikasi akun',
        html,
      });
      return ResponseHandler.jsonSuccess(res, {}, 'Link Verifikasi berhasil dikirim ulang ke email anda');
    } catch (error) {
      return ResponseHandler.serverError(res); 
    }
  }
}
