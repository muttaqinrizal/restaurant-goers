import { Request, Response, Router } from 'express';
import { isAuthenticated } from '../config/passport';

const dashboardController = Router();

const homeController = Router();
homeController.get('/', isAuthenticated, async (req: Request, res: Response) => {
  res.redirect('/dashboard');
});
homeController.use('/dashboard', isAuthenticated, dashboardController);
export { homeController };

